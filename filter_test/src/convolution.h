///////////////////////////////////////////////////////////////////////////////
// convolution.h
// =============
// convolution 1D and 2D
//
//  AUTHOR: Song Ho Ahn
// CREATED: 2005-07-18
// UPDATED: 2005-09-08
//
// Copyright (c) 2005 Song Ho Ahn
// http://songho.ca/dsp/convolution/convolution.html#cpp_conv1d
// Modifications made by Kevin Tchang 2019
///////////////////////////////////////////////////////////////////////////////

#ifndef CONVOLUTION_H
#define CONVOLUTION_H

#include <stdlib.h>
#include <math.h>
#include <stdio.h>



float* convolve1D(float *A, float *B, int lenA, int lenB);

double* impulseResponse(double fc, int total_time, int sampling_frequency);


#endif
