///////////////////////////////////////////////////////////////////////////////
// convolution.cpp
// ===============
// convolution 1D and 2D
//
//  AUTHOR: Song Ho Ahn
// CREATED: 2005-07-18
// UPDATED: 2005-09-12
//
// Copyright (c) 2005 Song Ho Ahn
// http://songho.ca/dsp/convolution/convolution.html#cpp_conv1d
// Modifications made by Kevin Tchang 2019
///////////////////////////////////////////////////////////////////////////////

#include "convolution.h"


///////////////////////////////////////////////////////////////////////////////
// 1D convolution
// We assume input and kernel signal start from t=0.
///////////////////////////////////////////////////////////////////////////////
float *convolve1D(float *A, float *B, int lenA, int lenB)
{
	int nconv;
	int i, j, i1;
	float tmp;
	float *C;

	//allocated convolution array
	nconv = lenA+lenB-1;
	C = (float*) calloc(nconv, sizeof(float));

	//convolution process
	for (i=0; i<nconv; i++)
	{
		i1 = i;
		tmp = 0.0;
		for (j=0; j<lenB; j++)
		{
			if(i1>=0 && i1<lenA)
				tmp = tmp + (A[i1]*B[j]);

			i1 = i1-1;
			C[i] = tmp;
		}
	}

	//return convolution array
	return(C);
}

// std::complex<double> *impulseResponse(double f1, double f2, int length)
// {
//     double w1 = 2*M_PI*f1;
//     double w2 = 2*M_PI*f2;

//     double wc = (w2 - w1) / 2;
//     std::complex<double> wshift(w1 + wc, 0);

//     std::complex<double> imaginary(0, 1);

//     std::complex<double> *h = (std::complex<double>*)calloc(length, sizeof(std::complex<double>));

//     h[0] = wc/M_PI;
// 	int i = 1;
//     for (double n = 0.001; n < 1; n+=0.001) {
// 		double a = sin(wc * n) / (M_PI*n);
// 		std::complex<double>A(a, 0);
// 		std::complex<double>N(n, 0);
//         h[i] = A * exp(imaginary*wshift*N);
// 		++i;
//     }

//     return h;
// }

double* impulseResponse(double fc, int total_time, int sampling_frequency)
{
    double wc = 2*M_PI*fc;

	double* h = (double*)calloc(total_time*sampling_frequency, sizeof(double));

    h[0] = 2*fc;

	double dt = 1.0/sampling_frequency;

	int i = 1;
    for (double n = dt; n < total_time*sampling_frequency; n+=dt) {
		double a = sin(wc * n) / (M_PI*n);
        h[i] = a;
		++i;
    }

    return h;
}
