#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

char ssid[] = "PePe Technologies";          // your network SSID (name)
char pass[] = "bunyim_like_grills";                    // your network password

WiFiUDP Udp;                                // A UDP instance to let us send and receive packets over UDP
const IPAddress outIp(192,168,43,172);        // remote IP of your computer
const unsigned int outPort = 20001;          // remote port to receive OSC
const unsigned int localPort = 20001;        // local port to listen for OSC packets (actually not used for sending)

char packetBuffer[1024]; //buffer to hold incoming packet
char  ReplyBuffer[] = "bunyim_like_grills";       // a string to send back
  
void printNetworkInfo() {
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(Udp.localPort());  
}

void setup() {
  Serial.begin(115200);
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
    // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  printNetworkInfo();
  
  display.setTextColor(WHITE);        
  display.setCursor(0,0); 
  
  display.clearDisplay();
  display.display();    
}

void doMeasurements() {
  
}

void loop() {  
  
  if (WiFi.status() != WL_CONNECTED) {
    display.clearDisplay();
    display.display();
    
    Serial.println("not connected");
    display.println(F("not connected"));
    display.display();
    doMeasurements();
  } else {
    // if there's data available, read a packet
    int packetSize = Udp.parsePacket();
    if (packetSize) {
      display.clearDisplay();
      display.display();
      
      display.setCursor(0,0); 
      
      Serial.print("Received packet of size ");
      Serial.println(packetSize);
      Serial.print("From ");
      IPAddress remoteIp = Udp.remoteIP();
      Serial.print(remoteIp);
      Serial.print(", port ");
      Serial.println(Udp.remotePort());
  
      // read the packet into packetBufffer
      int len = Udp.read(packetBuffer, 255);
      if (len > 0) {
        packetBuffer[len] = 0;
      }
      Serial.println("Contents:");
      Serial.println(packetBuffer);      
      
      display.println(packetBuffer);
      display.display();
      
      Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
      Udp.write(ReplyBuffer);
      Udp.endPacket();
    }
  }
}