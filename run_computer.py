#!/usr/bin/env python3
import socket

# constants
HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
# host may be 192.168.0.1, or whatever...
PORT = 65432        # Port to listen on (non-privileged ports are > 1023)

HEADER_SIZE = 0#50 # adjust to real header size
SIZE_STEP_DATA = 0#10
SIZE_LOCATION_DATA = 8 #20 #20 bytes per location

acknowledgement = 1000 #"OK"#0#bin(0000)

# framework for code taken from:
# https://realpython.com/python-sockets/#echo-server

# code get_ip() taken from:
#https://stackoverflow.com/questions/166506/finding-local-ip-addresses-using-pythons-stdlib
def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP
# prints location
def print_location(location_arr):
    print(location_arr[0]) #x/y
    print(location_arr[1]) #y/x
# sets up printing
def print_data(step, location_arr):
    print("Steps: " + str(step))
    print_location(location_arr)

# interprets the step data and location data
def interpret_data(header, step, location):
    # split location into two bits
    #location = int(location)
    # conversion may be iffy
    
    lower_bits = location >> int(SIZE_LOCATION_DATA/2)
    higher_bits = location - lower_bits
    location_arr = [higher_bits, lower_bits]
    print_data(step,location_arr)
    
# main of program
# setup a server on the wireless network
# the wireless network needs to be setup manually on the computer
print("Make sure the NodeMCU is connected onto the same wifi network.")
print("Please note that the ip of this computer on the network is: ")
print(HOST)
print(get_ip())
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen(0)
    conn, addr = s.accept()
    with conn:
        print('Connected by', addr)
        while True:
            data = conn.recv(1024)
            
            if not data:
                break # break if the connection is disrupted
            #conn.sendall(data)
            
            # get the header
            header = data[0:HEADER_SIZE]
            location = 0
            step = 0
            try:
                
                #extract location data and distance data
                step = data[HEADER_SIZE:HEADER_SIZE+SIZE_STEP_DATA]
                if step == '':
                    step = 0
                else:
                    step = float(step)
                location = data[HEADER_SIZE+SIZE_STEP_DATA:HEADER_SIZE+SIZE_STEP_DATA+SIZE_LOCATION_DATA]
                if location == '':
                    location = 0
                else:
                    location = float(location)
            except:
                print("Bad data")
            # interpret
            interpret_data(header, step, location)

            #input("Waiting for ENTER")                
            print("Sending acknowledgement")
            # send acknowledgement
            sent_bytes = conn.send(bytes("OK",'UTF-8'))#str#(acknowledgement)) #sendall
            print("Sent acknowledgement: " + str(sent_bytes) + " bytes")
            input("Waiting for ENTER")
            import os
            os.system("pause")
input("Waiting for ENTER")
