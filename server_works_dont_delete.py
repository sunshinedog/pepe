# PePe companion server
# Written by Nathaniel Seil, Kevin Tchang and Ivan Hu
# based on code from: https://pythontic.com/modules/socket/udp-client-server-example
#
import socket
import struct
from time import gmtime,strftime

localIP     = "127.0.0.1" # this line taken from: https://pythontic.com/modules/socket/udp-client-server-example
size_gps_struct = 28 # bytes
size_num_steps = 4 # bytes
size_gps_var = 4 # bytes
size_measurement_struct = 32 #bytes

# processes measurement data
def process_measurement(this_measurement, num_measurements):

    location_data_arr = []
    num_steps_arr = []
    # split everythign into 4 byte orderings
    j = 0
    month = 0
    day = 0
    hour = 0
    minute = 0
    second = 0
    latitude = 0.0
    longitude = 0.0
    num_steps = 0
    this_measurement_4_byte = []
    #this_measurement = this_measurement[82*4:]
    #print("all bytes")
    #print(this_measurement)
    for i in range(0,int(sizeMeasurements/4)):#int(sizeMeasurements/4)):
        this_measurement_4_byte.append(this_measurement[4*i:min(4*(i+1),sizeMeasurements)])
    
    for i in range(0,len(this_measurement_4_byte)):
        #print(this_measurement_4_byte[i][0])
        this_measurement_4_byte[i]= this_measurement_4_byte[i][::-1] # get order the right way round
        #print(this_measurement_4_byte[i])
        
        if j == 0:
                latitude = struct.unpack('>f',this_measurement_4_byte[i])
                latitude = latitude[0]
                #print("lat: " + str(latitude))
        elif j == 1:
                longitude = struct.unpack('>f',this_measurement_4_byte[i])
                longitude = longitude[0]
                #print("long: " + str(longitude))
        elif j == 2:
                month = int.from_bytes(this_measurement_4_byte[i],byteorder='big')
        elif j == 3:
                day = int.from_bytes(this_measurement_4_byte[i],byteorder='big')
        elif j == 4:
                hour = int.from_bytes(this_measurement_4_byte[i],byteorder='big')
        elif j == 5:
                minute = int.from_bytes(this_measurement_4_byte[i],byteorder='big')
        elif j == 6:
                second = int.from_bytes(this_measurement_4_byte[i],byteorder='big')
        elif j == 7:
                num_steps = int.from_bytes(this_measurement_4_byte[i],byteorder='big')#int.from_bytes(this_measurement_4_byte[i][0],byteorder='big')
        
        
        if j == 7: # last struct element
            j = 0
            #location_data_arr.append("M: " + str(month) + ",D: " + str(day) + ",H: " + str(hour) + ",m: " + str(minute) + ",s: " + str(second) + ",lat: " + str(latitude) + ",long: " + str(longitude)+" ,num steps: " + str(num_steps))
            location_data_arr.append("lat: " + str(latitude) + ",long: " + str(longitude)+", M: " + str(month) + ",D: " + str(day) + ",H: " + str(hour) + ",m: " + str(minute) + ",s: " + str(second)+", num steps: " + str(num_steps))
            #num_steps_arr.append(num_steps)
            
        else:
            j = j+1
        
    return location_data_arr,num_steps_arr

# stores data in text file for user.
def write_data(location_data):
    #https://stackoverflow.com/questions/415511/how-to-get-the-current-time-in-python
    f = open("data_"+strftime("%Y-%m-%d", gmtime())+".txt","a+") # open to append data
#    f.writeLine("\n")
    for i in range(0,len(location_data)):
        #f.write(num_steps[i]+"\n")
        f.write(location_data[i]+"\n")
    # all written
    f.write("\n")
    f.close()
    return

# debug function for sorting bit orderings
def printMeasurement(thisMeasurement):
    size = 20*size_measurement_struct #20 structs, 32 bytes each
    # big struct = thisMeasurement from bit 1 onwards
    big_struct = thisMeasurement[1:]
    big_struct = str(thisMeasurement[0:4]) + str('\0')
    print(big_struct)
    return

# debug function for sorting bit orderings.
def look_individual(this_measurement, num_measurements):
    print(int(this_measurement[sizeMeasurements-1]&this_measurement[sizeMeasurements-2]&this_measurement[sizeMeasurements-3]&this_measurement[sizeMeasurements-4]))
    print(float(this_measurement[sizeMeasurements-5]&this_measurement[sizeMeasurements-6]&this_measurement[sizeMeasurements-7]&this_measurement[sizeMeasurements-8]))
    print(float(this_measurement[sizeMeasurements-9]&this_measurement[sizeMeasurements-10]&this_measurement[sizeMeasurements-11]&this_measurement[sizeMeasurements-12]))
    return

# setup of server code taken from: https://pythontic.com/modules/socket/udp-client-server-example
# (no author given)
# the rest of the code (functions defined above, everything after comment:
# hand-shake done
# is written by: Nathaniel Seil, Kevin Tchang and Ivan Hu
########################################################################
# arbitrary port
# taken from: https://pythontic.com/modules/socket/udp-client-server-example
localPort   = 20001

bufferSize  = 1024
######################
# this section written by: Nathaniel Seil, Kevin Tchang, Ivan Hu
NUM_MEASUREMENTS = 20
sizeMeasurements = NUM_MEASUREMENTS *size_measurement_struct # in bytes

######################
# this sectiont taken from: https://pythontic.com/modules/socket/udp-client-server-example
msgFromServer       = "Hello UDP Client"

bytesToSend         = str.encode(msgFromServer)

 

# Create a datagram socket

UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

 

# Bind to address and ip

UDPServerSocket.bind(("", localPort))

 

print("UDP server up and listening")

 

# Listen for incoming datagrams

while(True):

    print("Waiting to receive handshake")
    bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)

    message = bytesAddressPair[0]

    address = bytesAddressPair[1]

    clientMsg = "Message from Client:{}".format(message)
    clientIP  = "Client IP Address:{}".format(address)
    
    print(clientMsg)
    print(clientIP)

   

    # Sending a reply to client

    UDPServerSocket.sendto(bytesToSend, address)
    print("Sent back message: "+msgFromServer)

# end section taken from: https://pythontic.com/modules/socket/udp-client-server-example
##########################################################################
    # hand-shake done.

    # code below here written by: Nathaniel Seil, Kevin Tchang and Ivan Hu
    
    # now receive all of the data from the esp8266
    print("Receiving measurement data from ESP")
    print("Size receive: " + str(sizeMeasurements+1))
    bytesReceivedPair = UDPServerSocket.recvfrom(sizeMeasurements+2) # null terminating bit
    thisMeasurement = bytesReceivedPair[0]
    #print("Now printing this measurement")
    #printMeasurement(thisMeasurement)
    #print("Done printing measurement")

    # this is a ghetto fix for the case where: broadcast into measurement receive
    while "bunyim" in str(thisMeasurement):
        # Sending a reply to client - second acknowledgement

        UDPServerSocket.sendto(bytesToSend, address)
        print("Sent back message: "+msgFromServer)
        print("Trying again to receive measurement data from ESP")
        bytesReceivedPair = UDPServerSocket.recvfrom(sizeMeasurements+2)
        thisMeasurement = bytesReceivedPair[0]
        #print("Now printing this measurement")
        #printMeasurement(thisMeasurement)
        #print("Done printing measurement")
    
    # and then store the data in a text file.
    #
    #print(thisMeasurement)
    print("processing")
    location_data,num_steps = process_measurement(thisMeasurement,NUM_MEASUREMENTS)
    write_data(location_data)
    #look_individual(thisMeasurement,NUM_MEASUREMENTS)
    print("Written")
    print("location data: ")
    for i in range(0,len(location_data)):
        print(location_data[i])
        print("\n")
    #print(location_data)
    print("num steps: ")
    for i in range(0,len(num_steps)):
        print(num_steps[i])
        print("\n")
    ##print(num_steps)

    #
    # Sending a reply to client - second acknowledgement

    UDPServerSocket.sendto(bytesToSend, address)
    print("Received data acknowledge: "+msgFromServer)
    
