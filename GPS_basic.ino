/*
 * OLED declarations
 */
#include <SPI.h>
#include <Arduino.h>
#include <U8x8lib.h>
U8X8_SSD1306_128X32_UNIVISION_SW_I2C screen(/* clock=*/ 5, /* data=*/ 4, /* reset=*/ -1);

/*
 * GPS declarations
 */
#include <SoftwareSerial.h>
#define SOFTWARE_RX_PIN 14
#define SOFTWARE_TX_PIN 12
SoftwareSerial softwareGPSSerial(SOFTWARE_RX_PIN, SOFTWARE_TX_PIN);
#include <TinyGPS++.h> // GPS parsing library
TinyGPSPlus gps;

/*
*  Interrupt Ticker declarations
*/
#include <Ticker.h>
Ticker gpsInterrupt;
volatile char boutTimeToCheckGPS = 0; // Ticker shouldn't perform callback blocking (such as read()) functions. Instead set a global flag and check the global flag in the main loop

/*
*  Testing declarations
*/
volatile int interruptCounter = 0;

void setup() {
  Serial.begin(115200);
  softwareGPSSerial.begin(9600);

  gpsInterrupt.attach(1.0, gpsRead);
  
  screen.begin();
  screen.setFont(u8x8_font_amstrad_cpc_extended_r);
}

void loop() { 
  // Keep updating the instantiated gps object with fresh data, but only do something with that data every 1 second
  while (softwareGPSSerial.available() > 0) {
    if (gps.encode(softwareGPSSerial.read())) {
      if (boutTimeToCheckGPS == 1) {
        gpsDisplayInfo();
      }
    }
  }
  
  Serial.print(F("."));
  delay(250);
}

void gpsRead() {
  boutTimeToCheckGPS = 1;
}

void gpsDisplayInfo() {
  if (gps.location.isValid())  {    
    Serial.print(F("\nLatitude: "));
    Serial.println(gps.location.lat(), 6);
    
    screen.drawString(0,0,"Latitude: ");
    screen.setCursor(0,1);
    screen.print(gps.location.lat(), 6);
    
    Serial.print(F("Longitude: "));
    Serial.println(gps.location.lng(), 6);
    
    screen.drawString(0,2,"Longitude:    ");
    screen.setCursor(0,3);
    screen.print(gps.location.lng(), 6);    
    
    Serial.print(F("Altitude: "));
    Serial.println(gps.altitude.meters());
  }
  else  {
    Serial.println(F("\nLocation: Not Available"));

  }
  
  Serial.print(F("Date: "));
  if (gps.date.isValid())  {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.println(gps.date.year());
  }
  else  {
    Serial.println(F("Not Available"));
  }

  Serial.print(F("Time (GMT): "));
  if (gps.time.isValid()) {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.println(gps.time.centisecond());
  }
  else  {
    Serial.println(F("Not Available"));
  } 
  
  Serial.print(F("Interrupts called: "));
  Serial.println(++interruptCounter);
  if (!gps.location.isValid()) {
    screen.setCursor(11,2);
    screen.print(interruptCounter);
  } else {
    screen.setCursor(11,2);
    screen.print(" ");
  }
  Serial.println();
  boutTimeToCheckGPS = 0;  
}
