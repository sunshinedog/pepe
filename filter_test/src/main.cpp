#include "convolution.h"
#include "Timer.h"

using namespace std;

int main()
{
    float in[5] = {1, 2, 3, 2, 1};
    float* out;
    float k[10] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    int i;
    Timer t;

    t.start();
    out = convolve1D(in, k, 5, 10);
    t.stop();

    printf("SUCCESS, Elapsed Time: %5.3f us\n", t.getElapsedTimeInMicroSec());

    printf("INPUT\n");
    for(i=0; i < 5; ++i)
    {
        printf("%5.2f, ", in[i]);
    }
    printf("\n\n");

    printf("KERNEL\n");
    for(i=0; i < 10; ++i)
    {
        printf("%5.2f, ", k[i]);
    }
    printf("\n\n");

    printf("OUTPUT\n");
    for(i=0; i < 14; ++i)
    {
        printf("%5.2f, ", out[i]);
    }
    printf("\n");


    double* h = impulseResponse(200, 4, 250);

    for (int i = 0; i < 1000; i++) {
        printf("%g\n", h[i]);
    }
    
    return 0;
}
