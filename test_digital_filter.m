omega_c = 180*pi;
omega_shift = 200*pi;
length = 250
h(1) = omega_c/pi;
for i = 2:length
    n = i/length;
    h(i) = sin(omega_c*n)/(pi*n);%   *exp(1j*omega_shift*n);
end

ns = [1:length]/length
close all
figure
plot(ns,real(h))
hold on
plot(ns,imag(h))
legend('real','imaginary')
title('filter in time domain')
xlabel('time')
ylabel('h[n]')
i = 0.001;
htest = sin(omega_c*i)/(pi*i)   *exp(1j*omega_shift*i)