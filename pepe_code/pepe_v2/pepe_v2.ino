

//TODO:
//1) migrate display over to new library - done?
//2) proc memory for storing data permanently on the board - done!
//3) adding peak detection - add Ivan's changes
//4) bit sizing for structs.- done!
//5) (optional) filtering of signal.
//6) sending data to computer.- done! Processing still needs to be done on computer side.
//6) General program stuff.
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <SPI.h>
//**********************************************************
//screen
#include <U8x8lib.h>
U8X8_SSD1306_128X32_UNIVISION_SW_I2C screen(/* clock=*/ 5, /* data=*/ 4, /* reset=*/ -1);
//U8X8_SSD1306_128X32_UNIVISION_SW_I2C screen = display; /*screen = &display;*/
//**********************************************************
//GPS
/*
 * GPS declarations
 */
#include <SoftwareSerial.h>
#define SOFTWARE_RX_PIN 14
#define SOFTWARE_TX_PIN 12
SoftwareSerial softwareGPSSerial(SOFTWARE_RX_PIN, SOFTWARE_TX_PIN);
#include <TinyGPS++.h> // GPS parsing library
TinyGPSPlus gps;
typedef float gps_var_lat_long; //8 bytes for double, 4 bytes for float.
typedef  int gps_var_time; //4 bytes
typedef  int step_var; //4 bytes
//#define PROGMEM PROGMEM; //type for program memor flag.


/*
*  Interrupt Ticker declarations
*/
#include <Ticker.h>
Ticker gpsInterrupt;
volatile char boutTimeToCheckGPS = 0; // Ticker shouldn't perform callback blocking (such as read()) functions. Instead set a global flag and check the global flag in the main loop

#define NUM_STRUCTS 20
//************************************************************
//structs to store data in memory
#define SIG_SIZE 1000 //1000 samples

double signal_data_x[SIG_SIZE];
double signal_data_y[SIG_SIZE];
double signal_data_z[SIG_SIZE];
float sumArr[SIG_SIZE];
volatile int signal_data_index = 0; //not needed but still useful.

/************************************************************/
//STORE IN MEMORY:
//store in memory so as to prevent our data being over-written after a reset.
 int interruptCounter = 0; //also acts as our index in the array of structs for data.

//for signal processing.
#define PEAK_THRESHOLD 15

float timerInterval = 10.0;

struct gps_data{
  gps_var_lat_long lat;
  gps_var_lat_long longitude;
  //float alt;

  gps_var_time month;
  gps_var_time day;
  //gps_var_time year;

  gps_var_time hour;
  gps_var_time minute;
  gps_var_time second;
  step_var num_steps;
};
struct big_struct{
  struct gps_data gps_dat[NUM_STRUCTS]; //7*4 = 28 bytes.
  //step_var num_steps[NUM_STRUCTS]; //4 bytes each
  
  //struct accelerometer_data[NUM_STRUCTS];
 
  //32 bytes per struct. 
  //optimisation: want: a power of two for the struct.


};
big_struct all_data;
//80 kiB for data. can store thousands...

//size of the struct is: 6 * sizeof(gps_var_time) + 2 * sizeof(gps_var_lat_long) = 6 * 4 + 2 * 4 = 32 bytes.
/************************************************************/

//************************************************************

/*
*  Testing declarations
*/
int testInt = 0;

volatile int stepCount = 0;

//**********************************************************
//networking ssid and password.
char ssid[] = "PePe Technologies";          // your network SSID (name)
char pass[] = "bunyim_like_grills";                    // your network password

WiFiUDP Udp;                                // A UDP instance to let us send and receive packets over UDP
const IPAddress outIp(192,168,43,255);        // remote IP of your computer
const IPAddress broadcastIP(255,255,255,255);//192,168,43,255);//(255,255,255,255); //broadcast IP
const unsigned int outPort = 20001;          // remote port to receive OSC
const unsigned int localPort = 20001;        // local port to listen for OSC packets (actually not used for sending)

char packetBuffer[1024]; //buffer to hold incoming packet
char  ReplyBuffer[] = "bunyim_like_grills";       // a string to send back
#define UPLOAD_SIZE sizeof(all_data) //(NUM_STRUCTS* (sizeof(step_var)+sizeof(gps_data)))
unsigned char uploadBuffer[UPLOAD_SIZE+1]; //plus one for null terminating character at end.
//networking stuff done.
//**********************************************************

//**********************************************************
/*
 * Accelerometer declarations
 */
int x;
int y;
int z;

//acceleromter
void xSetAxis() {
  digitalWrite(D6, LOW);
  digitalWrite(D7, LOW);
}

void ySetAxis() {
  digitalWrite(D6, LOW);
  digitalWrite(D7, HIGH);
}

void zSetAxis() {
  digitalWrite(D6, HIGH);
  digitalWrite(D7, LOW);
}

//GPS
void gpsStoreFlashMemory(){
  gps_var_lat_long lat = gps.location.lat();
  gps_var_lat_long longitude = gps.location.lng();
  //float alt = gps.altitude.meters();

  gps_var_time month = gps.date.month();
  gps_var_time day = gps.date.day();
  //gps_var_time year = gps.date.year();

  gps_var_time hour = gps.time.hour();
  gps_var_time minute = gps.time.minute();
  gps_var_time second = gps.time.second();

  //TODO: bit packing., choosing sizes...

  //store in proc_mem by setting these variables.
  all_data.gps_dat[interruptCounter].lat = lat;
  all_data.gps_dat[interruptCounter].longitude = longitude;
  all_data.gps_dat[interruptCounter].month = month;
  all_data.gps_dat[interruptCounter].day = day;
  //all_data.gps_dat[interruptCounter].year = year;
  all_data.gps_dat[interruptCounter].hour = hour;
  all_data.gps_dat[interruptCounter].minute = minute;
  all_data.gps_dat[interruptCounter].second = second;

}

void gpsStoreDummyData(){
  gps_var_lat_long lat = 100;
  gps_var_lat_long longitude = 200;
  //float alt = gps.altitude.meters();

  gps_var_time month = 300;
  gps_var_time day = 400;
  //gps_var_time year = gps.date.year();

  gps_var_time hour = 500;
  gps_var_time minute = 600;
  gps_var_time second = 700;

  //TODO: bit packing., choosing sizes...

  //store in proc_mem by setting these variables.
  all_data.gps_dat[interruptCounter].lat = lat;
  all_data.gps_dat[interruptCounter].longitude = longitude;
  all_data.gps_dat[interruptCounter].month = month;
  all_data.gps_dat[interruptCounter].day = day;
  //all_data.gps_dat[interruptCounter].year = year;
  all_data.gps_dat[interruptCounter].hour = hour;
  all_data.gps_dat[interruptCounter].minute = minute;
  all_data.gps_dat[interruptCounter].second = second;
}

void gpsDisplayInfo() {
  if (gps.location.isValid())  {
    Serial.print(F("Latitude: "));
    Serial.println(gps.location.lat(), 6);

    screen.drawString(0,0,"Latitude: ");
    screen.setCursor(0,1);
    screen.print(gps.location.lat(), 6);

    Serial.print(F("Longitude: "));
    Serial.println(gps.location.lng(), 6);

    screen.drawString(0,2,"Longitude: ");
    screen.setCursor(0,3);
    screen.print(gps.location.lng(), 6);

    Serial.print(F("Altitude: "));
    Serial.println(gps.altitude.meters());
  }
  else  {
    Serial.println(F("Location: Not Available"));
  }

  Serial.print(F("Date: "));
  if (gps.date.isValid())  {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.println(gps.date.year());
  }
  else  {
    Serial.println(F("Not Available"));
  }

  Serial.print(F("Time (GMT): "));
  if (gps.time.isValid()) {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.println(gps.time.centisecond());
  }
  else  {
    Serial.println(F("Not Available"));
  }

  Serial.print(F("Satellites: "));
  Serial.println(gps.satellites.value());
  Serial.println();
  Serial.println();
  delay(1000);
  //boutTimeToCheckGPS = 0;

  //Serial.print(F("Interrupts called: "));
  //Serial.println(++interruptCounter);
  //Serial.println(F("Index in flash memory: "));
  //Serial.println(interruptCounter);
  //transfer over to new library for OLED display.
  //TODO
  // if (!gps.location.isValid()) {
  //   screen.setCursor(11,2);
  //   screen.print(interruptCounter);
  // } else {
  //   screen.setCursor(11,2);
  //   screen.print(" ");
  // }
  // Serial.println();



}


void get_accelerometer_data(){
  for(int i = 0; i < SIG_SIZE; i++){


    xSetAxis();
    x = analogRead(A0);
    //screen.drawString(0,0, "X: ");
    //screen.setCursor(3,0);
    //screen.print(x);
    //Serial.print(x);
    //Serial.print(",");
    ySetAxis();
    y = analogRead(A0);
    //screen.drawString(0,1, "Y: ");
    //screen.setCursor(3,1);
    //screen.print(y);
    //Serial.print(y);
    //Serial.print(",");
    zSetAxis();
    z = analogRead(A0);
    //screen.drawString(0,2, "Z: ");
    //screen.setCursor(3,2);
    //screen.print(z);
    //Serial.print(z);
    //Serial.println();


    //delay(1); //1000 samples per second. NECESSARY!

    //store data - append to signal_data
    signal_data_x[i] = x;
    signal_data_y[i] = y;
    signal_data_z[i] = z;
    signal_data_index++;

    delay(1);
  }


}
//interrupt handler for GPS
void gpsRead() {
  boutTimeToCheckGPS = 1;
}

uint16_t get_steps_from_data(){
  //do signal processing on signal data.
  int num_samples = signal_data_index+1;
  //peak detection code insert here
  //****************************************
  //signal_data_x, signal_data_y, signal_data_z
  //size: SIG_SIZE
  uint16_t steps = 0;
  float xMean = 0;
  float yMean = 0;
  float zMean = 0;
  steps = 0; //steps within the current sample

  // 1000 samples, 3ms between samples: total sample time 3 seconds
  for (int i = 0; i < SIG_SIZE; i++) {
    //xSetAxis();

    xMean += signal_data_x[i];
    //ySetAxis();

    yMean += signal_data_y[i];
    //zSetAxis();

    zMean += signal_data_z[i];

    //delay(3);
  }

  xMean = (float) xMean/1000;
  yMean = (float) yMean/1000;;
  zMean = (float) zMean/1000;

  // remove mean
  for (int i = 0; i < SIG_SIZE; i++) {
    signal_data_x[i] -= xMean;
    signal_data_y[i] -= yMean;
    signal_data_z[i] -= zMean;
  }

  // add signals together
  float sumMean = 0;
  for (int i = 0; i < SIG_SIZE; i++) {
    sumArr[i] = signal_data_x[i] + signal_data_y[i] + signal_data_z[i];
    sumMean += sumArr[i];
  }
  sumMean = sumMean/SIG_SIZE;
  // calculate standard deviation
  float standardDeviation = 0;
  for (int i = 0; i < 1000; i++) {
      standardDeviation += pow(sumArr[i] - sumMean, 2);
  }
  standardDeviation = sqrt(standardDeviation/1000);
  standardDeviation = 1.75 * standardDeviation;

  // serial printing loop plus peak counting
  // only counts as a peak if the data goes above the standard deviation for a certain number of samples PEAK_THRESHOLD
  uint16_t currentPeakLength = 0;
  for (int i = 0; i < SIG_SIZE; i++) {
    // Serial.print(sumArr[i]); Serial.print(",");
    // Serial.print(sumMean); Serial.print(",");
    // Serial.println(standardDeviation);

    if (sumArr[i] > standardDeviation) {
      currentPeakLength++;
    }

    if (sumArr[i] < standardDeviation && currentPeakLength >= PEAK_THRESHOLD) {
      currentPeakLength = 0;
      steps++;
      //totalSteps++;
    } else if (sumArr[i] < standardDeviation && currentPeakLength < PEAK_THRESHOLD) {
      currentPeakLength = 0;
    }
  }

  //****************************************

  return steps;
}

void store_number_steps(){
  //store stepCount.
  all_data.gps_dat[interruptCounter].num_steps = stepCount;
  // print for debug purposes.
  //reset stepCount to 0 so that counted steps for next interval is correct.
  stepCount = 0;
  //then reset the signal_data
  /*for(int i =0; i < SIG_SIZE; i++){
    signal_data_x[i] = 0;
    signal_data_y[i] = 0;
    signal_data_z[i] = 0;
  }*/
  //above not necessary as there is no risk of having previous data affecting future results.
  //either over-written, or if not over-written then what happens is that in signal processing
  //if not all the data is utilised and input into signal_data then it won't be used for counting steps etc.
}
void write_data_memory(){
  //store number of steps first.
  store_number_steps();
  //gpsStoreFlashMemory();
  //gpsStoreDummyData();
  //since number steps has already been taken then increment interruptCounter
  //note: this is not necessarily the case?
  interruptCounter++;
  //reset signal data collection
  signal_data_index = 0;
}

void reset_memory(){
  for(int i = 0; i < NUM_STRUCTS; i++){
    all_data.gps_dat[i].num_steps = 0;
    all_data.gps_dat[i].lat = 0;
    all_data.gps_dat[i].longitude = 0;
    all_data.gps_dat[i].month = 0;
    all_data.gps_dat[i].day = 0;
    all_data.gps_dat[i].hour = 0;
    all_data.gps_dat[i].minute = 0;
    all_data.gps_dat[i].second = 0;
  }
}
IPAddress remoteIp;// = Udp.remoteIP();
uint16_t remotePortStore;

void broadcast(){
  //send to 255.255.255.255
  Serial.println("Broadcasting");
  //send back the acknowledgement.
  //broadcastIP
    Udp.beginPacket(broadcastIP, outPort);
    Udp.write(ReplyBuffer);
    Udp.endPacket();

    Serial.println("End broadcast");
}
int didntReceiveAnything = 0;
void receive_data(){
  Serial.println("Receive");
  int counter = 0;
  //try and receive data. time out after 10 seconds if nothing received.
  didntReceiveAnything = 0;
  while(counter < 10000){
    counter++;
    int packetSize = Udp.parsePacket();
    if (packetSize) {
      screen.clearDisplay();
      screen.display();
  
      screen.setCursor(0,0);
  
      Serial.print("Received packet of size ");
      Serial.println(packetSize);
      Serial.print("From ");
      remoteIp = Udp.remoteIP();
      Serial.print(remoteIp);
      Serial.print(", port ");
      remotePortStore = Udp.remotePort(); 
      Serial.println(remotePortStore);//Udp.remotePort());
  
      // read the packet into packetBufffer
      int len = Udp.read(packetBuffer, 255);
      if (len > 0) {
        packetBuffer[len] = 0;
      }
      //Serial.println("Contents:");
      //Serial.println(packetBuffer);
  
      //screen.println(packetBuffer); //display what was sent - should be the handshake
      //screen.display();
      didntReceiveAnything =0;
      break;
    }
    else{
      //Serial.println("Didn't receive anything");
      didntReceiveAnything=1;
    }
    delay(1);
  }
  if(didntReceiveAnything == 1){
    Serial.println("Didn't receive anything");
  }
  Serial.println("Done receive");
}

void send_data(){
  if(didntReceiveAnything == 1) {
    Serial.println("Skipping send");
    return;
  }
  
    Serial.println("Sending");
  //send back the acknowledgement.
    //Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    //Udp.write(ReplyBuffer);
    //Udp.endPacket();

    //now upload the data to the computer.
    Udp.beginPacket(broadcastIP,outPort);//Udp.remotePort());//Udp.remoteIP(),Udp.remotePort());
    for(int i = 0; i < UPLOAD_SIZE; i++){ //UPLOAD_SIZE
      uploadBuffer[i] = *((unsigned char*)&all_data+i);
      //if(i < 32 && i%4==0 && i!=0){
        //byte temp [] = ((byte)uploadBuffer[i]&(byte)uploadBuffer[i-1])&(byte)uploadBuffer[i-2]&(byte)uploadBuffer[i-3]));
      //Serial.print("uploadBuffer is: "); 
      //Serial.println(uploadBuffer[i],HEX);//String(temp);
      //}
    }
    /*int j = 0;
    String altogether = "";
    //send 8 strings - one for each float/integer
    for(int i = 0; i < NUM_STRUCTS*8; i+=8){
      uploadBuffer[i] = str(all_data.num_steps[j]);
      uploadBuffer[i+1] = all_data.gps_data[j].lat;
      j = j+1;
    }*/
    uploadBuffer[UPLOAD_SIZE] = '\0'; //null terminating character. UPLOAD_SIZE was the index.
    //Serial.println("uploadBuffer altogether: " + String((char*)uploadBuffer));
    Udp.write((byte*)uploadBuffer,UPLOAD_SIZE*sizeof(unsigned char)); 
    Udp.endPacket();
    Serial.println("Done sending");
    //Serial.println("DEBUG");
    //Serial.println("interrupt counter: " + String(interruptCounter));
    //Serial.println("Signal data index: " + String(signal_data_index));
    interruptCounter = 0;

    // reset memory
    reset_memory();
}

//**********************************************************
void printNetworkInfo() {
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting UDP");
  Udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(Udp.localPort());
}

//**********************************************************

void setup() {
  Serial.begin(115200);
  
  /*if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }*/
    // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");

  printNetworkInfo();

  broadcast(); //connect to server 
  receive_data(); //receive acknowledgement and get ip
  send_data();  // used for synchronisation
  receive_data();
  Serial.println("Done with networking");
  
  //setup GPS
  softwareGPSSerial.begin(9600);
  gpsInterrupt.attach(timerInterval, gpsRead); //setup the interrupt
  //setup screen.
  screen.begin();
  screen.setFont(u8x8_font_amstrad_cpc_extended_r);

  //setup Pins.
  pinMode(D6, OUTPUT); // Mux select bit A
  pinMode(D7, OUTPUT); // Mux select bit B
  pinMode(A0, INPUT); // Analog input (ADC)

  //reset memory
  reset_memory();
}

void loop() {

/*  Serial.println(sizeof(int));
  Serial.println(sizeof(float));
  
  Serial.println(sizeof(all_data));
  */
  //ivan's stuff put here.

  get_accelerometer_data();
  stepCount += get_steps_from_data();

  screen.drawString(0,0, "Step count: ");
  screen.setCursor(0,2);
  screen.print((int)stepCount);
  //Serial.println(stepCount);

  if(softwareGPSSerial.available() >0){
//  Serial.println("GPS data after measurement");
    if(gps.encode(softwareGPSSerial.read()) && boutTimeToCheckGPS) {  //keep updating data
      gpsStoreFlashMemory();
      gpsDisplayInfo();
    }
    
  }
  
  //THIS CAN STAY. THIS HOLDS THE TIMER INTERRUPT CODE.
  if (boutTimeToCheckGPS == 1){//  && softwareGPSSerial.available() > 0 && gps.encode(softwareGPSSerial.read()) ) {
        
        boutTimeToCheckGPS = 0;
        int counter = 0;
        /*while(softwareGPSSerial.available() <= 0){
          counter = counter+1;
          delay(1);
          if(counter == 10){
            Serial.println("GPS timed out");
            //return;
          }
        }*/
        Serial.println("We are now sending/writing data.");
        
        //if(gps.encode(softwareGPSSerial.read())){
          //Serial.println("Got a GPS signal successfully!");
          //if memory is less than 25% full just store.
          if(interruptCounter <= NUM_STRUCTS/10){
            Serial.println("Writing measurement");
            if(gps.encode(softwareGPSSerial.read())){ //call again to update.
              gpsStoreFlashMemory();
              gpsDisplayInfo();
            }
            write_data_memory();
          }
          
          else{ //if there is much data to send.
            //gpsDisplayInfo();
            //store to flash memory.
            /*
              if (WiFi.status() != WL_CONNECTED) {  //if there is no wifi
                screen.clearDisplay();
                screen.display();
    
                Serial.println("not connected");
                screen.println(F("not connected"));
                screen.display();
    
                //check if memory completely full
                if(interruptCounter >= NUM_STRUCTS){
                  //out of memory! exception
                  screen.println(F("Out of memory!"));
                  screen.println(F("Please connect to WiFi"));
                  screen.display();
                }
                else{ //this is if memory is not completely full but more than 25% full
                  //memory not completely full. then get measurements and store.
                  //get the accelerometer data over time.
                  //get_accelerometer_data();
                  
                  write_data_memory();
                  //send_data();
                }
              }
              */
              //else { //this is if there is wifi
                //we have connected to wifi.
                //TODO: change power management settings to RF working
    
                // if there's data available, read a packet from the computer for acknowledgement
                //check if memory completely full
                /*if(interruptCounter >= NUM_STRUCTS){
                  //out of memory! exception
                  screen.println(F("Out of memory!"));
                  screen.println(F("Please connect to WiFi"));
                  screen.display();
                }*/
                Serial.println("Sending data!");
                if(gps.encode(softwareGPSSerial.read())){ //call again to update.
                  gpsStoreFlashMemory();
                  gpsDisplayInfo();
                }
                write_data_memory();
                broadcast(); //connect to server 
                receive_data(); //receive acknowledgement and get ip
                send_data();
                receive_data();
    
                //TODO: change power management settings to Modem_sleep mode.
                //clear data memory.
             // }
          }
        //}
    
  }
}
