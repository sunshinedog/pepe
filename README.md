# PePe
Latest version:	pepe_v4.ino

The two files used in the demonstration are:

Client-side (run on ESP8266)	->	pepe_v4.ino

Server-side	(run on computer)	->	server_works_dont_delete.py

All the other files were used for testing purposes.

Please note this code uses the license shown in LICENSE.txt.
