/*
 * OLED Declarations
 */
#include <Arduino.h>
#include <SPI.h>
#include <U8x8lib.h>
U8X8_SSD1306_128X32_UNIVISION_SW_I2C screen(/* clock=*/ 5, /* data=*/ 4, /* reset=*/ -1);

/*
 * Accelerometer Declarations
 */
int xVector[5];
int yVector[5];
int zVector[5];

void setup() {
  pinMode(D6, OUTPUT); // Mux select bit A
  pinMode(D7, OUTPUT); // Mux select bit B
  pinMode(A0, INPUT); // Analog input (ADC)
  
  screen.begin();

  Serial.begin(115200);
}

void loop() {
  for (int i = 0; i < 5; i++) {
    xSetAxis();
    xVector[i] = analogRead(A0);
    ySetAxis();
    yVector[i] = analogRead(A0);
    zSetAxis();
    zVector[i] = analogRead(A0);
  }

  Serial.println();

//  screen.setCursor(0,0);
//  screen.print(F("X:"));
  Serial.print(F("X: "));
  for (int i = 0; i < 5; i++) {
//    screen.print(xVector[i]);
//    screen.print(F(","));
    Serial.print(xVector[i]);
    Serial.print(F(","));
  }

  screen.setCursor(0,1);
  screen.print(F("Y:"));
  Serial.print(F("\nY: "));
  for (int i = 0; i < 5; i++) {
//    screen.print(yVector[i]);
//    screen.print(F(","));
    Serial.print(yVector[i]);
    Serial.print(F(","));
  }
  
//  screen.setCursor(0,2);
//  screen.print(F("Z:"));
  Serial.print(F("\nZ: "));
  for (int i = 0; i < 5; i++) {
//    screen.print(yVector[i]);
//    screen.print(F(","));    
    Serial.print(zVector[i]);
    Serial.print(F(","));
  }

  Serial.println();
}

void xSetAxis() {
  digitalWrite(D6, LOW);
  digitalWrite(D7, LOW);
}

void ySetAxis() {
  digitalWrite(D6, LOW);
  digitalWrite(D7, HIGH);
}

void zSetAxis() {
  digitalWrite(D6, HIGH);
  digitalWrite(D7, LOW);
}
